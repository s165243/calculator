package main;

public class Calculator {

    public int Add(String input) throws NegationException {
        int result = 0;
        String delimiter = ",";

        if (input.isEmpty()) {
            return 0;
        }

        String inputCleaned = input.replace("\n", "");

        if (inputCleaned.length() > 3){
            if (inputCleaned.substring(0, 2).equals("//")) {
                delimiter = String.valueOf(inputCleaned.charAt(2));
                System.out.println("Different delimiter found: " + delimiter);
                inputCleaned = inputCleaned.substring(3);
            }
        }

        String[] numbers = inputCleaned.split(delimiter);

        for (String number : numbers) {
            int intIndex = Integer.parseInt(number);
            if (intIndex >= 0) {
                result += intIndex;
            } else {
                throw new NegationException(intIndex);
            }
        }
        return result;
    }

    public static class NegationException extends Exception {
        public NegationException(int input) {
            super("Negatives not allowed. Negative found: " + input);
        }
    }


}
